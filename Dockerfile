FROM alpine:3.13

ENV CALDERA=caldera
ENV CALDERA_PORT=9999
ENV RETHINK_HOST=rethinkdb
ENV ERRBODY_PIDS=4444


WORKDIR /usr/src/app

COPY caldera2.7-interop.py /usr/src/app/run.py
COPY requirements.txt /tmp/requirements.txt


RUN apk update && apk upgrade && \
    apk add python3 dumb-init py3-pip && \
    pip3 install --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain && \
    pip3 install -r /tmp/requirements.txt

ENTRYPOINT  ["/usr/bin/dumb-init", "--"]
CMD ["python3", "/usr/src/app/run.py"]

import requests
import json
import random
from base64 import b64encode, b64decode
from os import environ
from copy import deepcopy
from pprint import pprint
from time import time, sleep
from multiprocessing import Pool
from hashlib import sha256
from colorama import Fore as color

import brain
from brain import connect
from brain.static import RBJ, RBT, RPC
from brain.queries.writes import insert_jobs
from brain.queries.reads import get_plugin_command, get_targets
from brain.jobs import WAITING

server = environ.get('CALDERA', '127.0.0.1')
port = environ.get('CALDERA_PORT', '9999')
environ['RETHINK_HOST'] = environ.get("RETHINK_HOST", "127.0.0.1")
all_the_pids = environ.get('ERRBODY_PIDS', '4444')

caldera_server = f"http://{server}:{port}"
# Update this to a generic test for caldera being up
initial_request = f"{caldera_server}/"

# Its all about the *bacon/beacon
results_url = f"{caldera_server}/beacon"
get_next_instruction = f"{caldera_server}/beacon"


def verify_caldera_online():
    """
    Ping->Pong
    If Caldera is up and running we should register that we are an active agent
    :return:
    """
    alive_attempts = 0
    server_up = False
    if not server or not port:
        print(color.RED, "Requires Environment Variables:\n\t"
                         "'CALDERA' with IP Address [ex. 10.20.128.109]\n\t"
                         "'CALDERA_PORT' [ex. 9999]\n\t"
                         "'RETHINK_HOST' with IP ADDRESS [ex. 10.20.193.110]")
        exit(1)
    while alive_attempts < 5:
        try:
            pinged = requests.post(url=initial_request)
            assert pinged.status_code == 200
            return True

        except ConnectionError:
            print(color.RED, f"Cant Connect to Caldera.... Preparing ION Cannon to blast Caldera...")
            sleep(5)
            alive_attempts += 1

    print(color.RED, "[-] We Give Up ... Networking is Hard. [-]")
    return server_up


def get_new_targets(linked):
    """

    :param linked: set: Contains All Known Targets
    :return: list: New Targets
    """
    targets = get_targets()
    new_targets = []
    wanted = set([x.strip() for x in environ.get("WANTED_TARGETS", "").split(",") if x.strip()])
    if not wanted:  # pull from RPP
        wanted = set([
            x['Name']
            for x in
            RPC.filter({"State": "Available"}).filter({"ServiceID": ""}).pluck("Name").run(connect())
        ])
    for __ in targets:
        hashable = f"{__['PluginName']}:{__['Port']} {__['Location']}"
        if __["PluginName"] in wanted and hashable not in linked:
            new_targets.append(__)
            linked.add(hashable)

    if new_targets:
        print("New Targets From MCP:")
        for target in new_targets:
            print('\t' + target['Location'])

    return new_targets


def prep_agent_body(target):
    """
     Creates the custom body for the POST request to Caldera. This is used to track each agent in Caldera
     :param target:
     :return:
     """
    hashable = f"{target['id']}".encode()
    unique_paw = str(int(sha256(hashable).hexdigest()[:4], 16))
    agent_body = {"architecture": "amd64",
                  "exe_name": "ntdll.exe",
                  "executors": [target['PluginName']],
                  "group": "MCP",
                  "host": target['Location'],
                  "location": "unknown",
                  "paw": unique_paw,  # replace with PAW later
                  "pid": all_the_pids,
                  "platform": "mcp", #changed from MCP
                  "ppid": "4443",
                  "privilege": "User",
                  "server": caldera_server,
                  "username": "unknown"
                  }

    agent_body_data = json.dumps(agent_body).encode()
    agent_body_data_encoded = b64encode(agent_body_data)
    return agent_body, agent_body_data_encoded


def get_jobs_for_agent(agent_body_data_encoded):
    """
    Submit a POST request to Caldera asking for next instruction/job to execute
    :param agent_body_data_encoded:
    :return:
    """
    try:
        caldera_encoded_response = requests.post(url=get_next_instruction, data=agent_body_data_encoded)
        # print(caldera_encoded_response)
        caldera_decoded_response = b64decode(caldera_encoded_response.text).decode()
        agent_info = json.loads(caldera_decoded_response)
    except ConnectionError:
        print(color.RED, "We Got Problems! Cannot reach '/instructions' on server.")
        agent_info = {'instructions': '[]'}

    return agent_info


def convert_caldera_job_to_pcp_job(agent_info):
    """
    Decode JSON from Caldera and extract job from MCP
    :param agent_info:
    :return: dict: Job For MCP
    """
    next_job = deepcopy(agent_info)
    next_job['instructions'] = json.loads(next_job['instructions'])
    print(color.RED, "JOBS FROM CALDERA: ")
    for i in range(len(next_job['instructions'])):
        try:
            instruction = next_job['instructions'][i]
            decoded = json.loads(instruction)
            command = json.loads(b64decode(decoded['command']).decode())
            # Verify all the Jobs From Caldera
            if isinstance(command, list):
                print("\t\t", command[1])
            else:
                print("\t\t", command)

            next_job['instructions'][i] = {
                "id": decoded["id"],
                "command": command,
                "Brain_ID": "",
                "Brain_ID_Done": False,
                "Sent": False
            }
        except Exception as e:
            print(e)
            raise
            pprint(instruction)
    return next_job


def submit_pcp_jobs(almost, target, agent_body):
    """
    Insert Jobs From Caldera into MCP for execution
    :param almost:
    :param target:
    :param agent_body:
    :return:
    """
    timeshift = -999  # because we have no NTP
    for i in range(len(almost['instructions'])):
        cmd = almost['instructions'][i]['command']
        def single_job(cmd, counter=0):
            template = get_plugin_command(agent_body['executors'][0], cmd['CommandName'])
            for y in range(len(cmd['Inputs'])):
                template['Inputs'][y]['Value'] = cmd['Inputs'][y]['Value']
            return {
                "Status": WAITING,
                "StartTime": time() + timeshift + 0.01 * counter,
                "ExpireTime": time() + 60 * 100,  # 45 min max
                "JobTarget": target,
                "JobCommand": template
            }
        if isinstance(cmd, dict):
            jobs = [single_job(cmd)]
        elif isinstance(cmd, list):
            jobs = []
            counter = 0
            for one_cmd in cmd:
                jobs.append(single_job(one_cmd, counter=counter))
        timeshift += 1
        inserted_job = insert_jobs(jobs)
        almost['instructions'][i]['Brain_ID'] = inserted_job['generated_keys']
        if inserted_job['inserted'] > 0:
            for job in jobs:
                if job['JobCommand']['Inputs']:
                    print(color.GREEN, f"Inserted Job {inserted_job['generated_keys']} For : '{job['JobTarget']['Location']}' into Brain"                                                                                                                                      
                                       f"\n\t\t\tExecuting : {job['JobCommand']['Inputs'][0]['Value']}\n", color.RESET)


def monitor_job_status(almost, agent_body):
    for i in range(len(almost['instructions'])):
        if isinstance(almost['instructions'][i]['command'], list):
            looptimes = len(almost['instructions'][i]['command'])
        else:
            looptimes = 1

        for j in range(looptimes):
            if not almost['instructions'][i]['Brain_ID_Done']:
                almost['instructions'][i]['Brain_ID_Done'] = bool(
                    all([
                        bool(brain.queries.is_job_complete(brain_id)) for brain_id in almost['instructions'][i]['Brain_ID']
                    ])
                )

                # Update Caldera to Done for each job complete
                if almost['instructions'][i]['Brain_ID_Done'] and not almost['instructions'][i]['Sent']:
                    transfer_pcp_result_to_caldera(almost, agent_body)
                    almost['instructions'][i]['Sent'] = True

            # if looptimes > 1 then almost['instructions'][i]['command'][j] is a list
            if (looptimes > 1):
                # Display Pending Jobs
                if almost['instructions'][i]['command'][j]['Inputs'] and not almost['instructions'][i]['Brain_ID_Done']:
                    print(
                        f"Pending: {almost['instructions'][i]['Brain_ID']} ---> "                                                      
                        f"'{almost['instructions'][i]['command'][j]['CommandName']}' ( {almost['instructions'][i]['command'][j]['Inputs'][0]['Value']} ) ",
                    )
                # Some Jobs Dont Have Inputs; But we still need to display that were waiting
                elif not almost['instructions'][i]['Brain_ID_Done']:
                    print(
                        f"Pending: {almost['instructions'][i]['Brain_ID']} ---> Executing: {almost['instructions'][i]['command'][j]['CommandName']}",
                    )
            # almost['instructions'][i]['command'] is a dictionary
            else:
                # Display Pending Jobs
                if almost['instructions'][i]['command']['Inputs'] and not almost['instructions'][i]['Brain_ID_Done']:
                    print(
                        f"Pending: {almost['instructions'][i]['Brain_ID']} ---> "                                                      
                        f"'{almost['instructions'][i]['command']['CommandName']}' ( {almost['instructions'][i]['command']['Inputs'][0]['Value']} ) ",
                    )
                # Some Jobs Dont Have Inputs; But we still need to display that were waiting
                elif not almost['instructions'][i]['Brain_ID_Done']:
                    print(
                        f"Pending: {almost['instructions'][i]['Brain_ID']} ---> Executing: {almost['instructions'][i]['command']['CommandName']}",
                    )

    return bool(all([x['Brain_ID_Done'] for x in almost['instructions']]))


def wait_for_jobs(almost, agent_body):
    """

    :param almost:
    :return:
    """
    complete = monitor_job_status(almost, agent_body)
    while not complete:
        sleep(random.randint(30, 300)/10)  # sleep between 3 and 30 seconds
        complete = monitor_job_status(almost, agent_body)


def transfer_pcp_result_to_caldera(almost, agent_body):
    """
    Sending POST request to Caldera

    https://caldera.readthedocs.io/en/latest/How-to-Build-Agents.html#part-2


    :param almost:
    :param agent_body:
    :return:
    """
    for i in range(len(almost['instructions'])):
        if almost['instructions'][i]['Brain_ID_Done'] and not almost['instructions'][i]['Sent']:
            results_for_caldera = "".join([x for x in [
                brain.queries.get_output_content(brain_id, max_size=None)
                for brain_id in
                almost['instructions'][i]['Brain_ID']
            ] if x])

            encoded_results = b64encode(results_for_caldera.encode()).decode()
            caldera_result_dict = {
                'id': almost['instructions'][i]['id'],
                'output': encoded_results,
                'pid': agent_body['pid'],
                'status': '0'
            }
            outer_result_container = {
                "paw": agent_body['paw'],
                "results": [
                    caldera_result_dict
                ]
            }
            worth_it = b64encode(json.dumps(outer_result_container).encode())
            update_caldera = requests.post(results_url, data=worth_it)

            if update_caldera.status_code == 500:
                print(color.RED, '[-] We Got Problems! Server rejected our POST. [-]')
                print(color.RED, update_caldera.text)

            elif update_caldera.status_code == 200:
                cmd_lst = []
                if isinstance(almost['instructions'][i]['command'], list):
                    for brain_cmd in almost['instructions'][i]['command']:
                        cmdstr = "{cmdname}( {cmdargs} )".format(
                            cmdname=brain_cmd["CommandName"],
                            cmdargs=", ".join([cmd_input["Value"] for cmd_input in brain_cmd["Inputs"]])
                        )
                        cmd_lst.append(cmdstr)
                else:
                    brain_cmd = almost['instructions'][i]['command']
                    cmdstr = "{cmdname}( {cmdargs} )".format(
                        cmdname=brain_cmd["CommandName"],
                        cmdargs=", ".join([cmd_input["Value"] for cmd_input in brain_cmd["Inputs"]])
                    )
                    cmd_lst.append(cmdstr)
                    print(
                        color.GREEN,
                        "\tResults for:\n\t",
                        "\n\t\t".join(cmd_lst),
                        "\n\tsent to server",
                        color.RESET
                    )
                    print(color.GREEN, "\t\t" + str(update_caldera) + '\n', color.RESET)


def agent_interop(target, agent_body, agent_body_data_encoded):
    """
    Get Jobs for Agent and Submit to MCP
    :param target:
    :param agent_body:
    :param agent_body_data_encoded:
    :return:
    """
    random.seed()  # forked, need new seed probably
    while RBT.get(target['id']).run(
            connect()):  # die if MCP removes the target

        work = get_jobs_for_agent(agent_body_data_encoded)
        if work['instructions'] != '[]':
            print(color.CYAN, f"[*] Next Instruction For Agent : {target['Location']} [*]", color.RESET)
            agent_job = convert_caldera_job_to_pcp_job(work)
            # pprint(almost)
            submit_pcp_jobs(agent_job, target, agent_body)
            wait_for_jobs(agent_job, agent_body)
            # transfer_pcp_result_to_caldera(agent_job, agent_body)

        sleep(random.randint(60,
                             120) / 10)  # 6 to 12 seconds
    print(color.RED, f"Target Left : {target}", color.RESET)
    return target


def done_callback(target):
    hashable = f"{target['PluginName']}:{target['Port']} {target['Location']}"
    _linked_targets.remove(hashable)
    return


def pool_except_callback(ex):
    print("Uncaught exception, closing:\n")
    print(ex)


if __name__ == "__main__":
    verify_caldera_online()
    ppe = Pool(processes=200)
    _linked_targets = set()
    print(color.GREEN, "[*]...Waiting for instructions from Caldera...[*]", color.RESET)
    while True:
        _new_targets = get_new_targets(_linked_targets)
        for _target in _new_targets:
            _agent_body, _agent_body_data_encoded = prep_agent_body(_target)
            ppe.apply_async(agent_interop,
                            (_target, _agent_body, _agent_body_data_encoded),
                            callback=done_callback,
                            error_callback=pool_except_callback)
            # agent_interop(_target, _agent_body, _agent_body_data_encoded)  # single threaded
        sleep(random.randint(600, 1200) / 10)  # 1 to 2 minutes
